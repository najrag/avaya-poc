# Oceana

Oceana is een workspace applicatie gemaakt door Avaya en geschreven in AngularJs. In Oceana kunnen de volgende kanalen via standaard layouts afgehandeld worden:

1. Voice kanaal
1. Web chat kanaal
1. SMS kanaal
1. Email kanaal
1. Social kanaal
1. Outbound kanaal
1. Video kanaal
1. Co-browse kanaal

Bij de standaard kanalen worden 1 of meerdere widgets al ingeladen waardoor er beperkte integratie mogelijkheden zijn. Je kan de standaard widgets niet aanpassen of hergebruiken. Je kan alleen nieuwe widgets in een andere regio toevoegen. Deze widgets kunnen zich abonneren op gebeurtenissen van de standaard widgets. 

Je kan per kanaal aangeven welk tabbladengebruikt worden[^1]? Per tabblad kan je aangeven welke layout er gebruikt moet worden en welke widgets in het layout ingeladen moeten worden.

## Generiek kanaal

Avaya heeft het generiek kanaal geïntroduceerd om maatwerk kanaal afhandeling mogelijk te maken. Het generiek kanaal kan ingezet worden om taken via REST aan te maken en te verwijderen en via Oceana te routeren en af te handelen. De standaard widgets zijn hier niet beschikbaar en je kan hier alleen de maatwerk widgets inladen[^1].

Omdat TKP workspaces alleen wil gebruiken voor het routeren, accepteren en afhandelen van taken is het noodzakelijk om het generiek kanaal in te zetten voor het afhandelen van klant contacten.

Via een [REST interface](documenten/GenericChannel_3.6.0.0.pdf) kunnen genrieke taken aangemaakt en verwijderd worden.

Via de [widget API](documenten/Avaya_Workspaces_Widget_Framework_Documentation.pdf) kunnen widgets taken uitvoeren abonneren op events in Oceana.  



## REST

Taak aanmaken: POST naar GenericChannelAPI/api/createcontact

Schema:

``` json
{
    "type": "object",
    "required": [ "contactID", "ssoToken", "routePoint", "identifier" ],
    "properties": {
        "contactID": {
            "type": "string",
            "description": "Moet uniek zijn"
        },
        "ssoToken": {
            "type": "string"
        },
        "routePoint": {
            "type": "string"
        },
        "identifier": {
            "type": "string",
            "enum": [ "emailAddress", "telephoneNumber", "social" "account", "crmId" ]
        },
        "emailAddress": {
            "type": "string"
        },
        "telephoneNumber": {
            "type": "string"
        },
        "socialHandle": {
            "type": "string"
        },
        "socialPlatform": {
            "type": "string"
        },
        "accountType": {
            "type": "string"
        },
        "accountID": {
            "type": "string"
        },
        "crmId": {
            "type": "string"
        },        
        "attributes": {
            "type": "string",
            "description": "Komma gescheiden Key.Value. Key en Value moet bestaan in Oceana."
        },
        "workRequestID": {
            "type": "string",
            "description": "Geen idee wat dit is."
        },
        "priority": {
            "type": "integer",
            "multipleOf": 1,
            "minimum": 1,
            "maximum": 10
        },
        "firstName": {
            "type": "string"
        },
        "lastName": {
            "type": "string"
        },
        "extraData": {
            "type": "string",
            "description": "Geen idee wat dit is."
        },
        "destinationAddress": {
            "type": "string",
            "description": "Geen idee wat dit is."
        },
        "isUseRPAsCalledParty": {
            "type": "boolean",
            "description": "Geen idee wat dit is."
        }
    },
    "anyOf": [{
        "properties": {
            "identifier": { "const": "emailAddress" }
        },
        "required": ["emailAddress"]
    }, {
        "properties": {
            "identifier": { "const": "telephoneNumber" }
        },
        "required": ["telephoneNumber"]
    }, {
        "properties": {
            "identifier": { "const": "social" }
        },
        "required": ["socialHandle", "socialPlatform"]
    }, {
        "properties": {
            "identifier": { "const": "account" }
        },
        "required": ["accountType", "accountID"]
    }, {
        "properties": {
            "identifier": { "const": "crmId" }
        },
        "required": ["crmId"]
    }]
}
```

Payload voorbeeld:

``` json
{
    "contactID": "3c7774bb-fb83-46ec-823f-80c9c2e7bbe3",
    "ssoToken": "JhbGciOiJSUzI1NiJ9........",
    "routePoint": "SystemDefaultDigitalRoutepoint",
    "identifier": "emailAddress",
    "emailAddress": "piet.jansen@gmail.com",
    "attributes": "Kanaal.Email,Niveau.1,Klacht:Nee,Onderwerp.WaardeOverdracht,Taal.Nederlands",
    "priority": 1
}
```

Taak verwijderen: POST naar GenericChannelAPI/api/dropcontact

``` json
{
    "type": "object",
    "required": [ "contactID", "ssoToken" ],
    "properties": {
        "contactID": {
            "type": "string",
            "description": "Moet uniek zijn"
        },
        "ssoToken": {
            "type": "string"
        }
    }
}
```

[^1]: Klopt dit?