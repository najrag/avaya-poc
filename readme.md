# Architectuur

Dit document beschrijft de stappen die we hebben genomen om tot de doelarchitectuur te komen.

## Workflows

We hebben voor de volgende inkomende klant contacten de workflows samen met het BPM team uitgewerkt:

1. [Afhandelen brief](./img/workflow-brief.png)
1. [Afhandelen email](./img/workflow-email.png)
1. [Afhandelen chat](./img/workflow-chat.png)
1. [Afhandelen telefoongesprek](./img/workflow-telefoon.png)
1. [Afhandelen case](./img/workflow-case.png)

TKP is eigenaar van de workflow. Dit betekent dat, daar waar mogelijk, TKP de workflow opstart en beëindigd. Vanuit de TKP workflow kan er via REST uitgestapt worden naar andere processen.

TKP levert de statemachine. Alle relevante workflow informatie wordt in de statemachine opgeslagen op basis van de workflow identifier (WID). Op basis van de WID kan je via REST alle informatie opvragen. 

## Processen

Vanuit de workflows zijn de volgende processen naar boven gekomen die we via externe leveranciers willen gaan invullen:

1. [Inhoud anonimiseren](#inhoud-anonimiseren)
1. [Attributen vastellen](#inhoud-indexeren)
1. [Taak routeren](#taak-routeren)
1. [Taak afhandelen](#taak-afhandelen)

### Inhoud anonimiseren

In dit proces moet privacy gevoelige informatie automatisch verwijderd worden uit documenten, teksten en / of meta gegevens.

### Inhoud indexeren

In dit proces worden teksten, documenten en / of meta gegevens omgezet in attributen. De attributen worden onder andere gebruikt voor routering:

``` json
{
    "type": "object",
    "required": [ "merk", "fonds" ],
    "properties": {
        "merk": {
            "type": "string"
        },
        "fonds": {
            "type": "integer"
        },
        "afzender": {
            "type": "object",
            "required": [ "type" ],
            "properties": {
                "type": {
                    "type": "string",
                    "enum": [ "deelnemer", "werkgever" ]
                },
                "pensioennummer": {
                    "type": "integer"
                }
            }
        },
        "klacht": {
            "type": "boolean"
        },
        "onderwerp": {
            "type": "string",
            "enum": [ "" ]
        },
        "niveau": {
            "type": "integer"
        }
    }

}
```

### Taak routeren

Op basis van de attributen wordt een taak aangeboden aan groep medewerkers.

### Taak afhandelen

Een taak wordt binnen een workspace door een medewerker afgehandeld.

#### Workspace

Via een workspace worden taken aan een medewerker aangeboden. De medewerker kan een taak accepteren. Nadat een taak geaccepteerd is wordt er een tabblad actief. Op het tabblad wordt er een layout met widgets ingeladen waarbij de taak als context gezet wordt. 

Een integratie widget kan ingeladen worden. De context gegevens zijn beschikbaar inclusief WID. Op basis van de WID kan de widget, wanneer nodig, workflow gegevens via een REST interface ophalen. 

Op basis van de workflow gegevens wordt er een url opgebouwd die via een iframe wordt ingeladen.

[Window postmessage](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage) kan gebruikt worden om workspace events en iframe events uit te wisselen via de integratie widget. De integratie widget fungeert dan als een [ACL](https://docs.microsoft.com/nl-nl/azure/architecture/patterns/anti-corruption-layer) waardoor de TKP en workspace systemen niet van elkaar afhankelijk zijn. 

![Workspace](img/omnichannel-workspace.png)
<sub>afbeelding: workspace schematisch weergegeven<sub>